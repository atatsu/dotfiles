"""" Dependencies
" * nerd-fonts-complete (AUR)
" * alexanderjeurissen/ranger_devicons (for ranger... dev icons) 

" {{{ General shit
set encoding=utf-8 
set t_Co=256 " enable mouse support set mouse=a
" the FAQ says to use this option (`set termguicolors`) to use "true color" in the
" terminal but it only seems to mess the damn colors up instead
"set notermguicolors
set termguicolors

" display whitespace characters
setlocal listchars=tab:»·,trail:·,eol:¬,space:␣
set list

" tabs and shit
set softtabstop=2
set tabstop=2
set noexpandtab
set shiftwidth=2
set autoindent

" keep three lines below and above the cursor
set scrolloff=3

" buffers keep change history
set hidden

" new splits will be focused on bottom
set splitbelow
j
" new vsplits will be focused on right
set splitright
j
" Disable Background Color Erase (BCE) so that color schemes
" work properly when vim is used inside tmux and screen.
if &term =~ 'screen'
	set t_ut=
endif 

" filetype plugins
filetype on
filetype plugin on
filetype plugin indent on
" FIXME: move these all to `ftplugin`
" remove trailing whitespace from various filetypes
autocmd BufWritePre *.js :%s/\s\+$//e
autocmd BufWritePre *.vue :%s/\s\+$//e
autocmd BufWritePre *.html :%s/\s\+$//e
autocmd BufWritePre *.css :%s/\s\+$//e
autocmd BufWritePre *.py :call flake8#Flake8()
autocmd FileType dockerfile autocmd BufWritePre <buffer> :%s/\s\+$//e
"autocmd FileType html autocmd BufWritePre <buffer> call HtmlBeautify()

" display
set number
syntax on
hi clear SpellBad
hi SpellBad cterm=underline,bold ctermfg=magenta
set foldmethod=indent
set foldlevel=100
set nowrap
set hlsearch
set cursorline

" sytles the omnicomplete popup
highlight Pmenu ctermbg=235 cterm=NONE
highlight PmenuSel ctermbg=130 ctermfg=232 cterm=bold
" changes behavior of the <Enter> key when the popup is visible in that the
" Enter key will simply select the highlighted menu item, just as <C-Y> does
"inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" set current buffer as window title
set title

" autocomplete vim commands
set wildmenu

" displays the statusline always
set laststatus=2

set showmode

" refresh syntax highlighting
autocmd BufEnter,InsertLeave * :syntax sync fromstart

" use ag over grep
if executable('ag')
	set grepprg=ag\ --nogroup\ --nocolor
endif

" configure terminal colors to match those of termite's
let g:terminal_color_0  = '#171717'
let g:terminal_color_1  = '#d81765'
let g:terminal_color_2  = '#97d01a'
let g:terminal_color_3  = '#ffa800'
let g:terminal_color_4  = '#16b1fb'
let g:terminal_color_5  = '#ff2491'
let g:terminal_color_6  = '#0fdcb6'
let g:terminal_color_7  = '#ebebeb'
let g:terminal_color_8  = '#38252c'
let g:terminal_color_9  = '#ff0000'
let g:terminal_color_10 = '#76b639'
let g:terminal_color_11 = '#e1a126'
let g:terminal_color_12 = '#289cd5'
let g:terminal_color_13 = '#ff2491'
let g:terminal_color_14 = '#0a9b81'
let g:terminal_color_15 = '#f8f8f8'
" }}}

" {{{ Settings lifted from Coc's example config
" some servers have issues with backup files, see #649
set nobackup
set nowritebackup
" give more space for displaying messages
set cmdheight=2
" having longer updatetime (default is 4000 ms) leads to noticeable
" delays and poor user experience
set updatetime=300
" don't pass messages to |ins-completion-menu|
set shortmess+=c
" }}}

" {{{ File associations (that ain't standard) 
" }}}



" {{{ Mappings

" copy register easy mode
noremap <leader>y "+y
vnoremap <leader>y "+y

" ag prompt
command -nargs=+ -complete=file -bar Ag silent! grep! <args>|cwindow|redraw!  
"nnoremap <leader>G :Ag<space> 
" grep for word under cursor 
"nnoremap <leader>K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR> 

" display a list of buffers prompting for the number to switch to 
"nnoremap <F5> :buffers<CR>:buffer<Space>

" code folding toggle
nmap <CR> za
" the following prevents the mapped <CR> from interfering
" with selection of history items and jumping to errors
"":autocmd CmdwinEnter * nnoremap <CR> <CR>
:autocmd BufReadPost quickfix nnoremap <CR> <CR>

" provide a way to remind neovim wtf the whitespace chars are supposed to be
nnoremap <leader><F10> :setlocal listchars=tab:»·,trail:·,eol:¬,space:␣<CR>
" toggle whitespace
nnoremap <F10> :<C-U>setlocal list! list? <CR>
" I don't know wtf is going on but if I load one file type (like a js file)
" and then load a different file type (like a py file) the whitespace for the
" latter file is completely fucked. Remind neovim what whitespace chars should
" be whenever a file is opened
autocmd BufNewFile,BufRead * setlocal listchars=tab:»·,trail:·,eol:¬,space:␣


" paste mode toggle
nnoremap <F12> :set invpaste paste?<CR>
set pastetoggle=<F12>

" vim-bufkill
cabbrev bd :BD!
" when in terminal Ctrl-d calls :BD! rather than doing a normal shell exit
" (which preserves splits)
"tnoremap <C-d> <C-\><C-n>:BD!<CR>

" buffer sizing
map <silent> <A-h> <C-w>>
map <silent> <A-j> <C-w>-
map <silent> <A-k> <C-w>+
map <silent> <A-l> <C-w><

" buffer navigation
"map <silent> <C-h> <C-w>h <C-w>_
"map <silent> <C-k> <C-w>k <C-w>_
"map <silent> <C-l> <C-w>l <C-w>_
"map <silent> <C-j> <C-w>j <C-w>_
map <silent> <C-h> <C-w>h
map <silent> <C-k> <C-w>k
map <silent> <C-l> <C-w>l
map <silent> <C-j> <C-w>j

" nvim specific
if has('nvim')
	let g:python2_host_prog='/usr/bin/python2'
	"let g:python3_host_prog='/usr/bin/python'
	let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1
	"let g:loaded_python_provider = 1
" allows navigating out of the terminal
	tnoremap <C-h> <C-\><C-n><C-w>h
	tnoremap <C-j> <C-\><C-n><C-w>j
	tnoremap <C-k> <C-\><C-n><C-w>k
	tnoremap <C-l> <C-\><C-n><C-w>l
	let $NVIM_TUI_ENABLE_TRUE_COLOR=1
	" automatically enter insert mode when switching to a terminal buffer
	autocmd BufWinEnter,WinEnter term://* startinsert
	" don't display whitespace characters in the terminal
	autocmd TermOpen term://* set nolist
	autocmd BufWinEnter,WinEnter term://* set nolist
	" need this so that whitespace chars are displayed if a file is opened
	" via ranger
	autocmd BufAdd * set list
	" don't show whitespace for vim-fugitive windows
	autocmd BufWinEnter,WinEnter */.git/* set nolist
	" remove whitespace chars from NERDTree
	"autocmd BufWinEnter,WinEnter *NERD* set nolist
	" exclude terminal from buffer list autocmd TermOpen * set nobuflisted zsh 
	" nnoremap <silent> <F3> :terminal zsh<CR> 
	"noremap <silent> <F3> :terminal zsh<CR>
	" zsh - but split the window first
	"nnoremap <silent> <F3> :split<CR> :terminal zsh<CR>i
	" ranger
	"nnoremap <silent> <F4> :split<CR> :terminal ranger<CR>i
endif

" }}}



" {{{ plugin options
"
" airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

" buffergator
"map <leader>bn :BuffergatorMruCycleNext<CR>
"map <leader>bp :BuffergatorMruCyclePrev<CR>
let g:buffergator_viewport_split_policy = "L"
"let g:buffergator_hsplit_size = 15
let g:buffergator_vsplit_size = 120
let g:buffergator_show_full_directory_path = 0
let g:buffergator_autoexpand_on_split = 0
let g:buffergator_sort_regime = "basename"
let g:buffergator_suppress_keymaps = 1

" deoplete
" see if this improves performance with numirias/semshi
" this option is deprecated
" let g:deoplete#auto_complete_delay = 100
" enable deoplete at startup
let g:deoplete#enable_at_startup = 1

" jedi
" disable jedi's completion since `deoplete-jedi` is installed
"let g:jedi#completions_enabled = 0
let g:jedi#popup_select_first = 1
let g:jedi#use_splits_not_buffers = "left"

" NERDCommenter
let g:NERDDefaultAlign = 'left'

" nerdtree
let NERDTreeQuitOnOpen = 1
let NERDTreeWinSize = 50
let NERDTreeIgnore=['\.pyc$', '\.vim$', '\~$']
let NERDTreeHijackNetrw=1

" devicons
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:DevIconsEnableFolderOpenClose = 1
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols = {}
let g:WebDevIconsUnicodeDecorateFileNodesExtensionSymbols['vue'] = ''

" pymode
let g:pymode_options_max_line_length = 120
let g:pymode_options_colorcolumn = 1
"let g:pymode_virtualenv = 1
"let g:pymode_virtualenv_path = './virtualenv'
let g:pymode_rope = 0
" let g:pymode_lint_checkers = ['pylint', 'pyflakes', 'mccabe']
" pyflakes doesn't seem respect `from __future__ import annotations`
let g:pymode_lint_checkers = ['pylint', 'mccabe']
let g:pymode_lint_options_pep8 = {'max_line_length': g:pymode_options_max_line_length}
let g:pymode_lint_options_pylint = {'max-line-length': g:pymode_options_max_line_length}
let g:pymode_lint_ignore = ['W191']

" supertab
let g:SuperTabDefaultCompletionType = '<c-x><c-o>'
"let g:SuperTabDefaultCompletionType = "context"
"let g:SuperTabContextDefaultCompletionType = "<c-p>"
"let g:SuperTabCompletionContexts = ['s:ContextText', 's:ContextDiscover']
"let g:SuperTabContextDiscoverDiscovery = ["&omnifunc:<c-x><c-o>"]

" vim-colors-pencil 
let g:pencil_higher_contrast_ui = 1 

" vim-flake8
let g:flake8_cmd="/usr/bin/flake8"
let g:flake8_show_in_gutter=1
"let g:flake8_show_in_file=1

" goyo
let g:goyo_width = 120

" ale
"nmap <silent> <C-n> <Plug>(ale_next_wrap)
"nmap <silent> <C-p> <Plug>(ale_previous_wrap)
let g:ale_disable_lsp = 1

" }}}



" {{{ plugin mappings

" vim-floaterm
function! FloatingShell(position)
	call inputsave()
	let l:name = input('Name of shell: ')
	call inputrestore()
	let l:cmd = 'FloatermNew --wintype=floating --autoclose=2 --name=' . l:name . ' --title=' . l:name . ' --position=' . a:position
	if a:position !=? "center"
		let l:cmd .= ' --height=0.9 --width=0.3'
	endif
	let l:cmd .= ' name=' . l:name . ' zsh'
	execute l:cmd
endfunction

" center floating shell
nnoremap <silent> <F3>c :call FloatingShell('center')<CR>
" left floating shell
nnoremap <silent> <F3>l :call FloatingShell('left')<CR>
" right floating shell
nnoremap <silent> <F3>r :call FloatingShell('right')<CR>
" show all floaterm windows
nnoremap <silent> <leader>ftl :CocList --normal floaterm<CR>
" toggle active floating window
nnoremap <silent> <leader>ftt :FloatermToggle<CR>
" toggle active floating window while in the terminal
tnoremap <silent> <F3> <C-\><C-n>:FloatermToggle<CR>
"

" coc 
" use tab for trigger completion with characters ahead and navigate
inoremap <silent><expr> <TAB>
	\ pumvisible() ? "\<C-n>" :
	\ <SID>check_back_space() ? "\<TAB>" :
	\ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1] =~# '\s'
endfunction

" use <c-space> to trigger completion
inoremap <silent><expr> <c-space> coc#refresh()
" make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter
"inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
"      \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" GoTo code navigation
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" rename
nmap <leader>r <Plug>(coc-rename)

" use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

" formatting selected code
"xmap <leader>f <Plug>(coc-format-selected)
"nmap <leader>f <Plug>(coc-format-selected)

function! s:show_documentation()
	if (index(['vim', 'help'], &filetype) >= 0)
		execute 'h '.expand('<cword>')
	elseif (coc#rpc#ready())
		call CocActionAsync('doHover')
	else
		execute '!' . &keywordprg . " " . expand('<cword>')
	endif
endfunction

" highlight the symbol and its references when holding the cursor
autocmd CursorHold * silent call CocActionAsync('highlight')

" remap <C-f> and <C-b> for scroll float windows/popups
nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"

" coc-explorer
nmap <space>e :CocCommand explorer<CR>
" coc-lists
"map <F2> :CocList --normal buffers<CR>
" coc-fzf-preview
nnoremap <leader>G :<C-u>CocCommand fzf-preview.ProjectGrep<space>
nnoremap <leader>GG :<C-u>CocCommand fzf-preview.ProjectGrep "\b<C-R><C-W>\b"<CR>
nnoremap <silent> <leader>ff :<C-u>CocCommand fzf-preview.ProjectFiles<CR>
nnoremap <silent> <F2>  :CocCommand fzf-preview.Buffers<CR>
nnoremap <silent> a<F2> :CocCommand fzf-preview.AllBuffers<CR>
nmap <leader>f [fzf-p]
xmap <leader>f [fzf-p]
nnoremap <silent> [fzf-p]gs :<C-u>CocCommand fzf-preview.GitStatus<CR>
nnoremap <silent> [fzf-p]ga :<C-u>CocCommand fzf-preview.GitActions<CR>
nnoremap <silent> [fzf-p]bt :<C-u>CocCommand fzf-preview.BufferTags<CR>

" coc-prettier
command! -nargs=0 Prettier :CocCommand prettier.formatFile

" buffergator
"map <F2> :BuffergatorToggle<CR>
"map <leader><F2> :BuffergatorTabsOpen<CR>

" fzf
"map <leader>f :FZF<CR>

" goyo / limelight
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

" nerdtree
"map <leader>N :NERDTreeToggle<CR>
"
" tagbar
map <leader>tb :TagbarToggle<CR>
map <leader>tb :TagbarOpenAutoClose<CR>
map <leader>Tb :TagbarToggle<CR>

" supertab autocmd FileType * \ if &omnifunc != '' |
"  \   call SuperTabChain(&omnifunc, "<c-p>") |
"  \   call SuperTabSetDefaultCompletionType("<c-x><c-u>") |
"  \ endif

" python-syntax
"let python_highlight_all = 1
" }}}



" {{{ plugin overrides
" pymode and it's fuckin' spaces only shit
"autocmd FileType python call overrides#pymode()

" }}}



" {{{ vim-plug
call plug#begin('~/.local/share/nvim/plugged')

" --------------------------------------------
" -- Colors
" color scheme collection
Plug 'flazz/vim-colorschemes'
Plug 'rafi/awesome-vim-colorschemes'

" --------------------------------------------
" -- Navigation
" tree explorer 
"Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
" cli fuzzy finder
" note: don't forget to also install `the_silver_searcher`
"Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"Plug 'junegunn/fzf.vim'
" preserves splits when closing buffers
Plug 'qpkorr/vim-bufkill'
" buffergator
"Plug 'jeetsukumaran/vim-buffergator'
" plugin to toggle, display and navigate marks
Plug 'kshenoy/vim-signature'

" --------------------------------------------
" -- VCS
" git wrapper 
Plug 'tpope/vim-fugitive'
" shows git diff in the gutter
Plug 'airblade/vim-gitgutter'
" nerdtree git status
"Plug 'Xuyuanp/nerdtree-git-plugin'
" a git commit browser `:GV` to open commit browser `:GV!` only commits pertaining to current file
" `:GV?` fills location list with revisions of current file
" `:GV` or :GV? can be used in visual mode to track selected lines
" `o` or `<cr>` on commit to display contents
" `o` or `<cr>` on commits to display diff
" `O` opens new tab
" `gb` for `:Gbrowse`
" `]]` and `[[` to move between commits
" `.` to start command-line with `:Git [CURSOR] SHA`
" `q` to close
Plug 'junegunn/gv.vim'

" --------------------------------------------
" -- Bells and whistles
" powerline variant
Plug 'vim-airline/vim-airline'
" fancy start screen for vim
"Plug 'mhinz/vim-startify'
" tagbar
Plug 'majutsushi/tagbar'
" vista.vim
" Viewer & Finder for LSP symbols and tags
Plug 'liuchengxu/vista.vim'
" distraction-free writing in vim
Plug 'junegunn/goyo.vim'
" hyperfocus-writing in vim
Plug 'junegunn/limelight.vim'
Plug 'voldikss/vim-floaterm'

" --------------------------------------------
" -- General coding utilities
" A Vim plugin that manages your tag files
Plug 'ludovicchabant/vim-gutentags'
" automatically closes quotes, parens, brackets, etc.
Plug 'Raimondi/delimitMate'
" pairs of handy bracket mappings
Plug 'tpope/vim-unimpaired'
" plugin for intensely orgasmic commenting
Plug 'scrooloose/nerdcommenter'
" tab completion
"Plug 'ervandew/supertab'
" surround
Plug 'tpope/vim-surround'
" asynchronous keyword completion 
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"Plug 'Shougo/denite.nvim'
" Check syntax in Vim asynchronously and fix files,
" with Language Server Protocol (LSP) support
Plug 'dense-analysis/ale'
" A collection of language packs for Vim
Plug 'sheerun/vim-polyglot'

" --------------------------------------------
" -- Language specific shit
" Intellisense engine for Neovim, full languageserver protocol
" support as VSCode
" :CocInstall coc-pyright
" :CocInstall coc-json
" :CocInstall coc-tsserver
" :CocInstall coc-css
" :CocInstall coc-explorer
" :CocInstall coc-html
" :CocInstall coc-fzf-preview
" :CocInstall coc-lists
" :CocInstall coc-floaterm
"   - ripgrep
"   - bat (syntax colors in preview)
"   - fugitive
" :CocInstall coc-vetur
" :CocInstall coc-prettier
" :CocInstall coc-git
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" preview colors (hex)
Plug 'ap/vim-css-color'

" - Python
" Python syntax highlighting for Vim
"Plug 'hdima/python-syntax'
" Semantic Highlighting for Python in Neovim
Plug 'numirias/semshi', { 'do': ':UpdateRemotePlugins' }
" pymode
"Plug 'klen/python-mode', { 'branch': 'develop' }
" linter
Plug 'nvie/vim-flake8'
" jedi
" goto assignment - <leader>g
" goto definition - <leader>d
" goto (typing) stub <leader>s
" show docs - K
" rename - <leader>r
" usages - <leader>n
"Plug 'davidhalter/jedi-vim'
" deoplete and jedi
" Plug 'zchee/deoplete-jedi' 
"Plug 'deoplete-plugins/deoplete-jedi'

" - Web
" automatically closes html tags (and positions cursor center of tags
" Plug 'vim-scripts/HTML-AutoCloseTag'
" vastly improved javascript indentation and syntax support
" Plug 'pangloss/vim-javascript'
" html5 omnicomplete, indent, and syntax 
" Plug 'othree/html5.vim'
" syntax highlighting for a number of popular js libraries
" Plug 'othree/javascript-libraries-syntax.vim'
" syntax highlighting for Vue.js components
" Plug 'posva/vim-vue'
" TODO: don't forget to learn this one
" Plug 'mattn/emmet-vim'
" vim plugin which formats javascript files by js-beautify
"Plug 'maksimr/vim-jsbeautify'
" edit code that's embedded within other code
" Plug 'AndrewRadev/inline_edit.vim'
" A Vim plugin that provides GraphQL file detection, 
" syntax highlighting, and indentation.
" Plug 'jparise/vim-graphql'
" Tern-based javascript editing support
" XXX: Assumes you've ran `npm install -g tern` first!
" Plug 'ternjs/tern_for_vim', { 'do': 'npm install' }
" Language service for typescript
" syntax file (this is required for the typescript stuff apparently)
"Plug 'HerringtonDarkholme/yats.vim'
"Plug 'mhartington/nvim-typescript', {'do': './install.sh'}

" --------------------------------------------
" -- Configuration Syntax
" - Docker
" vim syntax file & snippets for Docker's Dockerfile
Plug 'ekalinin/Dockerfile.vim'
" - nginx
Plug 'chr4/nginx.vim'
" - PlantUML
Plug 'aklt/plantuml-syntax'

" --------------------------------------------
" Make sure this one loads last so that all plugins it
" supports have already been loaded
" Adds file type glyphs/icons to many popular vim plugins
Plug 'ryanoasis/vim-devicons'

call plug#end()
" }}}

set background=dark
colorscheme Benokai
"colorscheme alduin

" {{{ vim-signature keybindings
" mx           Toggle mark 'x' and display it in the leftmost column
" dmx          Remove mark 'x' where x is a-zA-Z
" 
" m,           Place the next available mark
" m.           If no mark on line, place the next available mark. Otherwise, remove (first) existing mark.
" m-           Delete all marks from the current line
" m<Space>     Delete all marks from the current buffer
" ]`           Jump to next mark
" [`           Jump to prev mark
" ]'           Jump to start of next line containing a mark
" ['           Jump to start of prev line containing a mark
" `]           Jump by alphabetical order to next mark
" `[           Jump by alphabetical order to prev mark
" ']           Jump by alphabetical order to start of next line having a mark
" '[           Jump by alphabetical order to start of prev line having a mark
" m/           Open location list and display marks from current buffer
" 
" m[0-9]       Toggle the corresponding marker !@#$%^&*()
" m<S-[0-9]>   Remove all markers of the same type
" ]-           Jump to next line having a marker of the same type
" [-           Jump to prev line having a marker of the same type
" ]=           Jump to next line having a marker of any type
" [=           Jump to prev line having a marker of any type
" m?           Open location list and display markers from current buffer
" m<BS>        Remove all markers
" }}}

" {{{ Resources
" deoplete and ultisnips - https://gregjs.com/vim/2016/neovim-deoplete-jspc-ultisnips-and-tern-a-config-for-kickass-autocompletion/
" }}}
