## Dependencies
 * inotify-tools
 * ueberzug
 * ncmpcpp
 * tmux

## Zsh Alias
`alias music='tmux new-session -s music "tmux source-file ~/.ncmpcpp/tmux_session"'`
