-- set vim options here (vim.<first_key>.<second_key> =  value)
return {
  opt = {
    relativenumber = true, -- sets vim.opt.relativenumber
    title = true,
  },
  g = {
    mapleader = " ", -- sets vim.g.mapleader
    -- sonokai_style = "andromeda",
  },
}
