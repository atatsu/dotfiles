require("trouble").setup {}

local which_key = require("which-key")
which_key.register({
  ["<leader>x"] = {
    name = "+Trouble",
    x = { "<cmd>TroubleToggle<cr>", "Toggle" },
    w = { "<cmd>Trouble workspace_diagnostics<cr>", "Workspace diagnostics" },
    d = { "<cmd>Trouble document_diagnostics<cr>", "Document diagnostics" },
    l = { "<cmd>Trouble loclist<cr>", "loclist" },
    q = { "<cmd>Trouble quickfix<cr>", "quickfix" },
  }
})
