vim.g.catppuccin_flavour = "macchiato"
require("catppuccin").setup {
  dim_inactive = {
    enabled = true,
    shade = "dark",
    percentage = 0.1
  },
  transparent_background = false,
  integrations = {
    neotree = {
      enabled = true,
      transparent_panel = true
    }
  }
}
