return function()
  -- rewire some of the default terminal bindings
  local map = vim.keymap.set
  local unmap = vim.keymap.del
  unmap("t", "<esc>")
  unmap("t",  "jk")
  unmap("n", "<c-q>")
  map("t", "<c-q>", "<c-\\><c-n>", { desc = "Terminal normal mode" })
  -- Set key binding
  -- Set autocommands
  vim.api.nvim_create_augroup("packer_conf", { clear = true })
  vim.api.nvim_create_autocmd("BufWritePost", {
    desc = "Sync packer after modifying plugins.lua",
    group = "packer_conf",
    pattern = "plugins.lua",
    command = "source <afile> | PackerSync",
  })

  -- Set up custom filetypes
  -- vim.filetype.add {
  --   extension = {
  --     foo = "fooscript",
  --   },
  --   filename = {
  --     ["Foofile"] = "fooscript",
  --   },
  --   pattern = {
  --     ["~/%.config/foo/.*"] = "fooscript",
  --   },
  -- }
end
